#
#  Makefile of g1a-wrapper tool.
#

.PHONY: all install clean mrproper

cc    = gcc
flags = -Iinclude -W -Wall
obj   = build/bmp_utils.o build/g1a-wrapper.o build/error.o
hdr   = include/bmp_utils.h include/g1a-wrapper.h include/error.h

output = build/g1a-wrapper

all: $(output)

install:
	cp $(output) ~/bin

build:
	mkdir -p $@

$(output): $(obj)
	$(cc) $^ -o $@ $(flags)

build/%.o: src/%.c $(hdr) | build
	$(cc) -c $< -o $@ $(flags)

clean:
	rm -rf build
distclean: clean
	rm -f $(output)
